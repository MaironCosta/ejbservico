package com.servico;

import javax.ejb.Local;

@Local
public interface ServicoBusinessLocal {
	
	public final static String LocalJNDIName = ServicoBusiness.class.getSimpleName() + "/local";

	String getTexto ();
	
}
